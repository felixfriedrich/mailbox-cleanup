# -*- coding: utf-8 -*-

import ConfigParser
import email
import getpass
import os
from clint.textui import progress
from imapclient import IMAPClient


def download_attachments(host, port, ssl, username, destination, password=None):
    print 'SSL', ssl
    server = IMAPClient(host, port, use_uid=True, ssl=ssl)
    if not password:
        password = getpass.getpass()
    server.login(username, password)
    server.select_folder('INBOX')

    print 'Downloading attachments:'

    messages = server.search(['NOT DELETED'])
    names = set()
    sender_list = set()
    sender_as_filename = set()

    for message_id in progress.mill(messages):
        response = server.fetch([message_id], ['RFC822'])

        for message_id, data in response.iteritems():
            try:
                message = email.message_from_string(data['RFC822'])
            except KeyError as e:
                print response
                print data
                print e

            from email.utils import parseaddr
            sender_string = message['From']

            if sender_string is None:
                name, sender = parseaddr(message['Sender'])
            else:
                name, sender = parseaddr(sender_string)

            if name is not None or not name == ' ':
                names.add(name)

            if not ' ' in sender and '@' in sender:
                sender_list.add(sender)
                sender_as_filename.add(normalize_filename(sender))

            process_message(message, destination)

    fp = open('sender.list', 'w')
    fp.write('\n'.join(sender_list))
    fp = open('sender_as_filename', 'w')
    fp.write('\n'.join(sender_as_filename))
    fp = open('names', 'w')
    fp.write('\n'.join(names))




def process_message(message, destination, dry_run=False):
    if message.is_multipart():
        for i in range(0, len(message.get_payload())):
            payload = message.get_payload(i)
            process_message(payload, destination)
    else:
        if message.get_filename() is not None:
            payload = message.get_payload(decode=True)

            filename = message.get_filename()
            normalized_filename = normalize_filename(filename)

            if not dry_run:
                # TODO check if file already exists
                fp = open(destination + normalized_filename, 'wb')
                fp.write(payload)


def normalize_filename(filename):
    import string

    whitelist = string.ascii_letters
    whitelist += string.digits
    whitelist_ = whitelist + '._-'

    if not '.' in filename:
        filename = filter(lambda c: c in whitelist_, filename)
        if len(filename) > 0:
            return filename
        else:
            import uuid
            return str(uuid.uuid4())
    else:
        name, suffix = filename_as_tuple(filename)
        suffix = filter(lambda c: c in whitelist, suffix)
        name = filter(lambda c: c in whitelist_, name)
        return '.'.join([name, suffix])


def filename_as_tuple(filename):
    if '.' in filename:
        splitted_filename = filename.split('.')
        suffix = splitted_filename[-1]
        name = '.'.join(splitted_filename[0:-1])
        return name, suffix
    else:
        return filename, ''

