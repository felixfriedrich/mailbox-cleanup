# -*- coding: UTF-8 -*-

import sys
import os

from mailbox_cleanup.util import download_attachments


OK = 0
ACCOUNT_DOES_NOT_EXIST = 5
IMAP_ERROR = 6


def main():
    args = commandline_arguments()

    if args.download_attachments:
        download_attachments(args.host, args.port, args.ssl, args.username, args.folder, args.password)
        sys.exit(OK)
    else:
        print 'Please use --help to list options'
        sys.exit(OK)


def commandline_arguments():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--download-attachments', action='store_true')
    parser.add_argument('-n', '--dry-run', action='store_true', help='do not perform actions. e.g. saving attachments')
    parser.add_argument('--host', type=unicode, help='host of IMAP server')
    parser.add_argument('--port', type=int, default=143, help='port of IMAP server')
    parser.add_argument('--ssl', action='store_true', default=False, help='use SSL')
    parser.add_argument('-u', '--username', type=unicode, help='username for IMAP login')
    default_folder = '~/attachments/'
    parser.add_argument('-f', '--folder', type=unicode, help='folder to save attachments. default: {}'.format(default_folder), default=default_folder)
    parser.add_argument('-p', '--password', type=unicode, default=None, help='passwort for IMAP login. default: ask on commandline')
    args = parser.parse_args()
    args.folder = os.path.expanduser(args.folder)
    return parser.parse_args()


if __name__ == '__main__':
    main()
