# -*- coding: utf-8 -*-

import unittest
from mailbox_cleanup.util import normalize_filename
from mailbox_cleanup.util import filename_as_tuple


class TestNormalizationFunctions(unittest.TestCase):

    def setUp(self):
        pass

    def test_no_slashed(self):
        filename = 'foo/bar.jpg'
        normalized = normalize_filename(filename)
        self.assertFalse('/' in normalized)

    def test_no_whitespace(self):
        filename = 'foo  bar.jpg'
        normalized = normalize_filename(filename)
        self.assertFalse(' ' in normalized)

    def test_no_special_characters(self):
        filename = 'föö_böö.jpg'
        normalized = normalize_filename(filename)
        self.assertFalse('ö' in normalized)

    def test_no_invalid_suffix(self):
        filename = 'foo.mp$'
        normalized = normalize_filename(filename)
        self.assertEqual(normalized, 'foo.mp')

    def test_multiple_dots(self):
        filename = 'boo.bar.jpg'
        normalized = normalize_filename(filename)
        self.assertEqual(normalized, filename)

    def test_filename_with_one_dots(self):
        filename = 'foo.jpg'
        name, suffix = filename_as_tuple(filename)
        self.assertEqual(name, 'foo')
        self.assertEqual(suffix, 'jpg')

    def test_filename_with_two_dots(self):
        filename = 'foo.bar.jpg'
        name, suffix = filename_as_tuple(filename)
        self.assertEqual(name, 'foo.bar')
        self.assertEqual(suffix, 'jpg')

    def test_filename_with_side_by_side_dots(self):
        filename = 'foo..jpg'
        name, suffix = filename_as_tuple(filename)
        self.assertEqual(name, 'foo.')
        self.assertEqual(suffix, 'jpg')

    def test_filename_with_no_dots(self):
        filename = 'foo'
        name, suffix = filename_as_tuple(filename)
        self.assertEqual(name, 'foo')
        self.assertEqual(suffix, '')

    def test_no_filename(self):
        filename = ''
        normalized = normalize_filename(filename)
        self.assertTrue(len(normalized) > 1)


if __name__ == '__main__':
    unittest.main()
