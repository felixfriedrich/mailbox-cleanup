from setuptools import setup, find_packages

setup(
    name='mailbox-cleanup',
    version='0.2',
    author='Felix Friedrich',
    author_email='info@felix-friedrich.de',
    url='https://bitbucket.org/felixfriedrich/mailbox-cleanup',
    license='GPL v2',
    description='Mailbox cleanup commandline tool',
    long_description=open('README.md').read(),
    packages=find_packages(),
    entry_points={
        'mailbox_cleanup': [
            'mailbox_cleanup = mailbox_cleanup',
        ],
        'console_scripts': [
            'mailbox-cleanup = mailbox_cleanup.mailbox_cleanup_cli:main',
        ]
    },
    install_requires=[
        'imapclient',
        'argparse',
        'clint'
    ],
)
